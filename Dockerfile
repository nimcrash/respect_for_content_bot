FROM python:3.7-slim
#FROM python:3.6-alpine
WORKDIR /src

RUN apt-get update && apt-get install bash
#RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . /src
RUN python setup.py develop

#EXPOSE 80

CMD ["rfc"]
