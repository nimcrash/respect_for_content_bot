from .base import Base
from .util import engine
from .bot import create_bot

def main():

    Base.metadata.create_all(engine)

    bot = create_bot()
    bot.start()

if __name__ == '__main__':
    main()
