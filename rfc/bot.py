import logging
from . import config

from datetime import datetime

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, ConversationHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from .util import Session, get_post, rate_post, \
    likes_count, dislikes_count, get_user, create_user, \
    is_user_subscribed

from .models import Post, Rating, User

from validators.url import url

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

confirm_reply_markup = InlineKeyboardMarkup([[InlineKeyboardButton(f'Отправить', callback_data = 'confirm_send')]])


LINK, DESCRIPTION = range(2)

class Bot:

    def __init__(self):

        self.updater = Updater(config.Configuration.API_KEY)
        self.updater.dispatcher.add_error_handler(error)
        self.channel_id = config.Configuration.CHANNEL_ID

    def start(self):
        self.updater.start_polling()
        self.updater.idle()      

def create_bot():

    bot = Bot()

    dp = bot.updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CallbackQueryHandler(callback_handler))

    dp.add_error_handler(error)

    post_handler = ConversationHandler(
        entry_points=[CommandHandler('post', post)],

        states={
            LINK: [MessageHandler(Filters.text, add_link, pass_user_data=True), CommandHandler('cancel', cancel, pass_user_data=True)],
            DESCRIPTION: [MessageHandler(Filters.text, add_description, pass_user_data=True), CommandHandler('cancel', cancel, pass_user_data=True)]
        },

        fallbacks=[CommandHandler('cancel', cancel, pass_user_data=True)]
    )

    dp.add_handler(post_handler)

    return bot

def start(bot, update):

    session = Session()

    user = get_user(session, update.message.from_user.id)

    if user is None:
        create_user(session, update.message.from_user.id, update.message.from_user.name)

    session.commit()

    bot.send_message(chat_id = update.message.chat_id, text = "Welcome!")

def help(bot, update):
    bot.send_message(chat_id = update.message.chat_id, text = 'Help!')

def post(bot, update):

    session = Session()
    user = get_user(session, update.message.from_user.id)
    session.close()

    if user is None:
        update.message.reply_text('Сначала выполни /start')
        return ConversationHandler.END

    if not is_user_subscribed(bot, config.Configuration.CHANNEL_ID, user.tg_user_id):
        update.message.reply_text('Постинг разрешен только подписчкам канала.')
        return ConversationHandler.END

    if not user.can_post():       
        update.message.reply_text('Недостаточно постов.') 
        return ConversationHandler.END

    update.message.reply_text('Введи ссылку')

    return LINK

def add_link(bot, update, user_data):

    text = update.message.text

    if not url(text):
        update.message.reply_text('Это не ссылка, попробуй снова.')
        return LINK

    user_data['link'] = text

    update.message.reply_text('Введи описание')

    return DESCRIPTION

def add_description(bot, update, user_data):

    text = update.message.text

    if url(text):
        update.message.reply_text('Введи описание, а не ссылку!')
        return DESCRIPTION

    update.message.reply_text('Твоё сообщение будет выглядеть следующим образом: \n')

    msg = text + '\n \n' + user_data['link']
    del user_data['link']

    def message_to_kwargs(msg):
            return {
                'text': msg or '',
                'parse_mode': 'HTML',
                'disable_web_page_preview': True,
                'reply_markup': confirm_reply_markup
            }

    bot.send_message(chat_id = update.message.chat_id, **message_to_kwargs(msg))

    return ConversationHandler.END 

def cancel(bot, update, user_data):
    
    user_data.clear()

    update.message.reply_text('Постинг отменен.')

    return ConversationHandler.END 

def error(bot, update):
    logger.warning('Update "%s" caused error "%s"', bot, update.error)

def callback_handler(bot, update):

    query = update.callback_query

    if query.data == 'confirm_send':
        
        session = Session()

        user = get_user(session, query.from_user.id)

        if user is None:
            query.message.reply_text('Сначала выполни /start')
            session.close()
            return

        if not user.can_post():       
            query.message.reply_text('Недостаточно постов.')
            session.close() 
            return

        def message_to_kwargs(msg):
            return {
                'text': msg or '',
                'parse_mode': 'HTML',
                'disable_web_page_preview': True,
                'reply_markup': make_reply_markup()
            }

        message = bot.sendMessage(chat_id = config.Configuration.CHANNEL_ID, **message_to_kwargs(query.message.text))

        session.add(Post(user_id = user.id, 
            text = query.message.text,
            date = datetime.now(),
            tg_message_id = message.message_id,
            archived = False,
            deleted = False
            )
        )

        user.available_posts -= 1

        session.commit()

        bot.send_message(
            chat_id = query.message.chat_id,
            text = 'Сообщение отправлено.')

    elif query.data == 'like':
    	rate_this_post(bot, update)
        
    elif query.data == 'dislike':
    	rate_this_post(bot, update, False)
        
        
def rate_this_post(bot, update, like = True):

    query = update.callback_query

    session = Session()

    post = get_post(session, query.message.message_id)
    if post.archived:
        session.close()
        bot.answer_callback_query(update.callback_query.id, "Post archived")
        return

    user = get_user(session, query.from_user.id)

    rate_post(session, post.id, user.id, like)

    likes = likes_count(session, post.id)
    dislikes = dislikes_count(session, post.id)   

    session.commit()

    bot.answer_callback_query(update.callback_query.id, 'liked' if like else 'disliked')

    query.edit_message_reply_markup(
        reply_markup = make_reply_markup(likes, dislikes))

def make_reply_markup(likes=0, dislikes=0):
    return InlineKeyboardMarkup([
        [InlineKeyboardButton(f'{likes} 👍', callback_data='like'), InlineKeyboardButton(f'👎 {dislikes}', callback_data='dislike')],
    ])