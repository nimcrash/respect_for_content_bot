from celery import Celery
from .config import Configuration
from celery.schedules import crontab

app = Celery('rfc', broker = Configuration.CELERY_BROKER,
    include = ['rfc.tasks'])

app.conf.timezone = 'UTC'
app.conf.enable_utc = True
app.conf.beat_schedule = {
    'archive_posts': {
        'task': 'rfc.tasks.archive_posts',
        'schedule': crontab(minute = 0, 
            day_of_week = '*/%s' % Configuration.ARCHIVE_POSTS_EVERY_X_DAYS) 
    },
    'update_available_posts': {
        'task': 'rfc.tasks.update_available_posts',
        'schedule': crontab(minute = 3,
            day_of_week = '*/%s' % Configuration.UPDATE_AVAILABLE_POSTS_EVERY_X_DAYS)
    },
    'create_digest': {
        'task': 'rfc.tasks.create_digest',
        'schedule': crontab(minute = 2,
            day_of_week = '*/%s' % Configuration.CREATE_DIGEST_EVERY_X_DAYS)
    },
    'delete_shitpost': {
        'task': 'rfc.tasks.delete_shitpost',
        'schedule': crontab(minute = '*/%s' % Configuration.DELETE_SHITPOST_EVERY_X_MIN)
    }
}


if __name__ == '__main__':
    app.start()