import logging
from confi import BaseEnvironConfig, IntConfig, ConfigField, FloatConfig, \
    BooleanConfig


class Configuration(BaseEnvironConfig):
    
    API_KEY = ConfigField(required=True)
    CHANNEL_ID = ConfigField(required=True)
    CHANNEL_NAME = ConfigField()
    ADMIN_USER_ID = ConfigField(required=True)

    SQLALCHEMY_DATABASE_URI = ConfigField(required=True)
    CELERY_BROKER = ConfigField(required=True)

    ARCHIVE_POSTS_EVERY_X_DAYS = FloatConfig(default = 1)
    UPDATE_AVAILABLE_POSTS_EVERY_X_DAYS = FloatConfig(default = 1)
    CREATE_DIGEST_EVERY_X_DAYS = FloatConfig(default = 1)
    DELETE_SHITPOST_EVERY_X_DAYS = FloatConfig(default = 0.99)

    DELETE_SHITPOST_LIKE_DIFFERENCE = IntConfig(default = 10)