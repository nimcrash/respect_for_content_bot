from sqlalchemy import Column, Integer, String, DateTime, Boolean, \
    ForeignKey, select
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

from .util import Session
from .base import Base

class Post(Base):

    __tablename__ = 'post'

    id = Column(Integer, primary_key = True)

    user_id = Column(Integer, ForeignKey('user.id'))
    
    text = Column(String)
    date = Column(DateTime)
    archived = Column(Boolean)
    tg_message_id = Column(Integer)
    deleted = Column(Boolean)

    ratings = relationship('Rating', back_populates = 'post')
    user = relationship('User', back_populates = 'posts')

class Rating(Base):

    __tablename__ = 'rating'

    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey('user.id'))
    post_id = Column(Integer, ForeignKey('post.id'))

    like = Column(Boolean)
    dislike = Column(Boolean)
    
    post = relationship('Post', back_populates = 'ratings')

class User(Base):

    __tablename__ = 'user'

    id = Column(Integer, primary_key = True)

    tg_user_id = Column(Integer)
    tg_user_name = Column(String)
    available_posts = Column(Integer)

    posts = relationship('Post', back_populates = 'user')

    def can_post(self):
        return self.available_posts > 0



