from .celery import app
from .util import Session
from .models import Post, User, Rating
from .bot import create_bot

from sqlalchemy import func, case, desc
from datetime import datetime, timedelta
from time import sleep

from .config import Configuration

@app.task
def archive_posts():

    session = Session()

    posts = session.query(Post).filter(Post.archived == False, ).all()

    for post in posts:
        post.archived = True

    session.commit()

@app.task
def update_available_posts():
   
    session = Session()
    users = session.query(User).filter(User.available_posts == 0).all()

    for user in users:
        user.available_posts = 1

    session.commit()

    bot = create_bot().updater.bot
    for user in users:
        bot.sendMessage(chat_id = user.tg_user_id, text = 'Количество доступных постов обновлено. Доступных постов: 1')

@app.task
def create_digest():
    
    now = datetime.now()

    session = Session()

    posts = session.query(
        Post.text,
        User.tg_user_name,
        func.sum(
            case([(Rating.like == True, 1)], else_=0) - 
            case([(Rating.dislike == True, 1)], else_=0)).label('difference')).\
        join(Post.ratings).\
        join(Post.user).\
        filter(Post.date.between(now - timedelta(7), now), Post.deleted == False).\
        group_by(Post.text, User.tg_user_name).\
        order_by(desc('difference')).\
        limit(7).\
        all()

    session.close()

    if posts:

        text = 'Лучшие посты за неделю. \n \n'
        number = 1

        for post in posts:

            text += '%s. %s \n\n%s \n\n' % (number, post.tg_user_name, post.text)
            number += 1

        botty = create_bot()
        bot = botty.updater.bot
        bot.sendMessage(chat_id = botty.channel_id, text = text, disable_web_page_preview = True)

@app.task
def delete_shitpost():

    now = datetime.now()

    session = Session()

    difference = func.sum(
        case([(Rating.like == True, 1)], else_=0) - 
        case([(Rating.dislike == True, 1)], else_=0)).label('difference')

    posts = session.query(
        Post).\
        join(Post.ratings).\
        filter(Post.date.between(now - timedelta(7), now), Post.deleted == False).\
        group_by(Post).\
        having(difference <= -1 * Configuration.DELETE_SHITPOST_LIKE_DIFFERENCE).\
        order_by(difference).\
        limit(7).\
        all()

    if not posts:
        session.close()
        return

    botty = create_bot()
    bot = botty.updater.bot

    for post in posts:
        
        bot.edit_message_text(chat_id = botty.channel_id, message_id = post.tg_message_id,
            text = 'del')

        post.deleted = True

        sleep(2)

    session.commit()