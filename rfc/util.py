from . import config
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

engine = create_engine(config.Configuration.SQLALCHEMY_DATABASE_URI)
Session = sessionmaker(bind=engine)

from .models import Post, Rating, User
#Posts
def get_post(session, tg_message_id):
    return session.query(Post).filter_by(tg_message_id = tg_message_id).one_or_none()

def rate_post(session, post_id, user_id, like):

    rating = session.query(Rating).\
        filter(Rating.post_id == post_id, Rating.user_id == user_id).one_or_none()

    if rating is None:
        rating = Rating(user_id = user_id,
            post_id = post_id,
            like = like,
            dislike = not like)

        session.add(rating)
        
    else:
        rating.like = True if rating.like == False and like == True else False
        rating.dislike = True if rating.dislike == False and like == False else False

def likes_count(session, post_id):
    return session.query(Rating).filter(Rating.post_id == post_id, Rating.like == True).count()

def dislikes_count(session, post_id):
    return session.query(Rating).filter(Rating.post_id == post_id, Rating.dislike == True).count()

#Users
def get_user(session, tg_user_id):
    return session.query(User).filter_by(tg_user_id = tg_user_id).one_or_none()

def create_user(session, tg_user_id, tg_user_name):
    session.add(User(tg_user_id = tg_user_id,
        tg_user_name = tg_user_name,
        available_posts = 1))    

#Other
def is_user_subscribed(bot, channel_id, user_id):
    try:
        chat_member = bot.get_chat_member(channel_id, user_id)
    except TelegramError as e:
        if "User_id_invalid" in e.message:
            return False
        else:
            raise
    if not chat_member:
        return False

    if chat_member.status not in ('member', 'administrator', 'creator'):
        return False

    return True
        